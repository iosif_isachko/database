// Require
const Sequelize = require('sequelize');
const Model 	= Sequelize.Model;

// Music Database
const year 		= require('./year');
const genre 	= require('./genre');
const country 	= require('./country');
const musician 	= require('./musician');
const album 	= require('./album');
const song 		= require('./song');
const sg 		= require('./sg');
const log 		= require('./log');


// Models
class Year 		extends Model { }
class Genre 	extends Model { }
class Country 	extends Model { }
class Musician 	extends Model { }
class Album 	extends Model { }
class Song 		extends Model { }
class SG 		extends Model { }
class Log 		extends Model { }

class MusicSequelize {

	constructor() {
		this.sequelize 	= null;
		this.Year 		= null;
		this.Genre 		= null;
		this.Country 	= null;
		this.Musician 	= null;
		this.Album 		= null;
		this.Song 		= null;
		this.SG 		= null;
		this.Log 		= null;
	}

	async init(config) {
		this.sequelize = await new Sequelize(config.database, config.user, config.password, {
			dialect: config.dialect,
			host: config.host,
			// logging: false,
			define: {
				timestamps: false,
				freezeTableName: true,
			},
		});

		// Init
		this.Year 		= Year.init(year, { sequelize: this.sequelize, modelName: "years" });
		this.Genre 		= Genre.init(genre, { sequelize: this.sequelize, modelName: "genres" });
		this.Country 	= Country.init(country, { sequelize: this.sequelize, modelName: "countries" });
		this.Musician 	= Musician.init(musician, {
			sequelize: this.sequelize,
			modelName: "musicians",
			uniqueKeys: {
				actions_unique: {
					fields: ['name', 'birthday', 'countryId']
				}
			}
		});
		this.Album 		= Album.init(album, {
			sequelize: this.sequelize,
			modelName: "albums",
			uniqueKeys: {
				actions_unique: {
					fields: ['title', 'musicianId']
				}
			}
		});
		this.Song 		= Song.init(song, { sequelize: this.sequelize, modelName: "songs", });
		this.SG 		= SG.init(sg, { sequelize: this.sequelize, modelName: "sg", });
		this.Log 		= Log.init(log, { sequelize: this.sequelize, modelName: "logs", });

		// Relationship
		await this.Country.hasMany(this.Musician, { foreignKey: 'countryId' });

		await this.Year.hasMany(this.Album, { foreignKey: 'yearId' });
		await this.Musician.hasMany(this.Album, { foreignKey: 'musicianId' });
		
		await this.Year.hasMany(this.Song, { foreignKey: 'yearId' });
		await this.Album.hasMany(this.Song, { as: 'album_song', foreignKey: 'albumId' });
		
		await this.Song.belongsToMany(this.Genre, { foreignKey: 'songId', through: this.SG });
		await this.Genre.belongsToMany(this.Song, { foreignKey: 'genreId', through: this.SG });
		
		await this.Song.belongsTo(this.Year);
		await this.Song.belongsTo(this.Album);
		await this.Album.belongsTo(this.Year);
		await this.Album.belongsTo(this.Musician);
	}

	async start(isForce = true) {
		try {
			await this.sequelize.sync({ force: isForce }) 
		} catch (err) {
			console.log(err)
		}
	}

}

module.exports = MusicSequelize;