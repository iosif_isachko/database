
const Sequelize = require('sequelize');


const Year = {
	id: {
		type: Sequelize.INTEGER,
		allowNull: false,
		primaryKey: true,
		autoIncrement: true,
	},
	year: {
		type: Sequelize.INTEGER,
		allowNull: false,
		unique: true,
	},
};


module.exports = Year;
