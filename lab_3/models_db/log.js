
const Sequelize = require('sequelize');


const Log = {
	id: {
		type: Sequelize.INTEGER,
		allowNull: false,
		primaryKey: true,
		autoIncrement: true,
	},
	text: {
		type: Sequelize.TEXT,
		allowNull: false,
	},
	date: {
		type: Sequelize.DATE,
		allowNull: false,
	},
};


module.exports = Log;