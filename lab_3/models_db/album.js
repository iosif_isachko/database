
const Sequelize = require('sequelize');

const Album = {
	id: {
		type: Sequelize.INTEGER,
		allowNull: false,
		primaryKey: true,
		autoIncrement: true,
	},
	title: {
		type: Sequelize.TEXT,
		allowNull: false,
	},
	yearId: {
		type: Sequelize.INTEGER,
		references: {
			model: 'years',
			key: 'id',
		},
		allowNull: true,
		onUpdate: "NO ACTION",
		onDelete: "NO ACTION",
	},
	musicianId: {
		type: Sequelize.INTEGER,
		references: {
			model: 'musicians',
			key: 'id',
		},
		allowNull: false,
		onUpdate: "NO ACTION",
		onDelete: "CASCADE",
	},
};


module.exports = Album;