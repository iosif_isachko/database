
const Sequelize = require('sequelize');


const Song = {
	id: {
		type: Sequelize.INTEGER,
		allowNull: false,
		primaryKey: true,
		autoIncrement: true,
	},
	title: {
		type: Sequelize.TEXT,
		allowNull: false,
	},
	yearId: {
		type: Sequelize.INTEGER,
		references: {
			model: 'years',
			key: 'id',
		},
		allowNull: true,
		onUpdate: "NO ACTION",
		onDelete: "SET NULL",
	},
	albumId: {
		type: Sequelize.INTEGER,
		references: {
			model: 'albums',
			key: 'id',
		},
		allowNull: false,
		onUpdate: "NO ACTION",
		onDelete: "CASCADE",
	},
};


module.exports = Song;