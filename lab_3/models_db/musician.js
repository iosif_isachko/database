
const Sequelize = require('sequelize');


const Musician = {
	id: {
		type: Sequelize.INTEGER,
		allowNull: false,
		primaryKey: true,
		autoIncrement: true,
	},
	name: {
		type: Sequelize.TEXT,
		allowNull: false,
	},
	birthday: {
		type: 'TIMESTAMP WITHOUT TIME ZONE',
		allowNull: true,
	},
	countryId: {
		type: Sequelize.INTEGER,
		references: {
			model: 'countries',
			key: 'id',
		},
		allowNull: true,
		onUpdate: "NO ACTION",
		onDelete: "SET NULL",
	},
};


module.exports = Musician;