
const Sequelize = require('sequelize');


const Genre = {
	id: {
		type: Sequelize.INTEGER,
		allowNull: false,
		primaryKey: true,
		autoIncrement: true,
	},
	genre: {
		type: Sequelize.TEXT,
		allowNull: false,
		unique: true,
	},
};


module.exports = Genre;