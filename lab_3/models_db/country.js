
const Sequelize = require('sequelize');


const Country = {
	id: {
		type: Sequelize.INTEGER,
		allowNull: false,
		primaryKey: true,
		autoIncrement: true,
	},
	country: {
		type: Sequelize.TEXT,
		allowNull: false,
		unique: true,
	},
};


module.exports = Country;