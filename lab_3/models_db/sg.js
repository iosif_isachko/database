
const Sequelize = require('sequelize');


const SG = {
	songId: {
		type: Sequelize.INTEGER,
		references: {
			model: 'songs',
			key: 'id',
		},
		onUpdate: "NO ACTION",
		onDelete: "CASCADE",
	},
	genreId: {
		type: Sequelize.INTEGER,
		references: {
			model: 'genres',
			key: 'id',
		},
		onUpdate: "NO ACTION",
		onDelete: "CASCADE",
	},
};


module.exports = SG;