// Model: Song

class Song {
	constructor(id, musician, album, title, year, genres) {
		this.id = id;
		this.musician = musician;
		this.album = album;
		this.title = title;
		this.year = year;
		this.genres = genres;
	}
}

module.exports = Song;