
# Лабораторна робота - 3

--- 

## "Засоби оптимізації роботи СУБД PostgreSQL"
## "Варіант № 5"

--- 

### Структура таблиць бази даних 

1. **years**
	- id 			| PK 		| serial 	| not null	|			|
	- year 			| 			| integer 	| not null 	| unique	|

2. **countries**
	- id 			| PK 		| serial 	| not null	|			|
	- country 		| 			| text 		| not null 	| unique	|

3. **genres**
	- id 			| PK 		| serial 	| not null	|			|
	- country 		| 			| text 		| not null 	| unique	|

4. **musicians**
	- id 			| PK 		| serial 	| not null	|
	- countryId		| FK		| 	 		| null 		|
	- name		 	| 			| text 		| not null 	|
	- birthday 		| 			| timestamp	| null 		|

	- *UNIQUE -> (country_id, name, birthday)*

5. **albums**
	- id 			| PK 		| serial 	| not null	|
	- yearId		| FK		| 	 		| null 		|
	- musicianId	| FK		| 	 		| not null 	|
	- title 		| 			| text		| not null 	|

	- *UNIQUE -> (musician_id, title)*

6. **songs**
	- id 			| PK 		| serial 	| not null	|
	- yearId		| FK		| 	 		| null 		|
	- albumId		| FK		| 	 		| not null 	|
	- title 		| 			| text		| not null 	|

7. **sg**
	- songId		| FK		| 	 		| not null 	|
	- genreId		| FK		| 	 		| not null 	|

	- *PK -> (song_id, genre_id)*

8. **logs**
	- id 			| PK 		| serial 	| not null	|			|
	- text	 		| 			| text 		| not null 	| unique	|
	- date			|			| date		| not null	|			|
	
---