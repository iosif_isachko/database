//

const MusicSequelize = require('../models_db/music_sequelize');
const Song = require('../models/song');


class Music {

	constructor() {
		this.db = new MusicSequelize();
	}

	async start(config) {
		await this.db.init(config);
		await this.db.start(false);
	}

	async getAll() {
		/* Build a query behind such a scheme
		
			SELECT DISTINCT songs.id, musicians.name as musician, albums.title as album, songs.title, years.year,
			array_agg(genres.genre) over (partition by sg.song_id order by sg.song_id rows between unbounded preceding and unbounded following) as genres
			FROM songs
			INNER JOIN albums ON songs.album_id = albums.id
			INNER JOIN years ON songs.year_id = years.id
			INNER JOIN musicians ON albums.musician_id = musicians.id
			INNER JOIN sg ON songs.id = sg.song_id
			INNER JOIN genres ON sg.genre_id = genres.id
			ORDER BY songs.id;
		*/
		try {
			const result = await this.db.Song.findAll(this.params());
			const music = await parseSongs(result);
			return music;
		} catch (err) {
			console.log(`Error getAll():\n${err}`)
		}
	}

	async getById(id) {
		try {
			const result = await this.db.Song.findAll(this.params(id));
			const song = await parseSongs(result);
			return song;
		} catch (err) {
			console.log(`Error getById(id):\n${err}`)
		}
	}

	async insert(song) {
		try {
			const year = await this.db.Year.findOne({ where: { year: song.year } } );
			const year_id = year == null ? (await this.db.Year.create({ year: song.year, })).dataValues.id : year.dataValues.id;

			const musician = await this.db.Musician.findOne({ where: { name: song.musician } });
			const musician_id = musician == null ? (await this.db.Musician.create({ 
				name: song.musician,
				birthday: new Date(),
				countryId: 2,
			})).dataValues.id : musician.dataValues.id;

			const album = await this.db.Album.findOne({ 
				where: { 
					title: song.album, 
					musicianId: musician_id, } 
			});
			const album_id = album == null ? (await this.db.Album.create({
				title: song.album,
				yearId: year_id,
				musicianId: musician_id,
			})).dataValues.id : album.dataValues.id;

			const new_song = await this.db.Song.findOne({ where: { title: song.title, albumId: album_id } });
			if (new_song) { return 0; }
			const new_song_id = (await this.db.Song.create({
				title: song.title,
				yearId: year_id,
				albumId: album_id,
			})).dataValues.id;

			for (const g of song.genres) {
				const genre = await this.db.Genre.findOne({ where: { genre: g } });
				const genre_id = genre == null ? (await this.db.Genre.create({ genre: g })).dataValues.id : genre.dataValues.id;
				await this.db.SG.create({
					songId: new_song_id,
					genreId: genre_id,
				});
			}
			return new_song_id;
		} catch (err) {
			console.log(`Error insert(song):\n${err}`)
		}
	}

	async removeById(id) {
		try {
			const result = await this.db.Song.destroy({
				where: {
					id,
				}
			});
			return result;
		} catch (err) {
			console.log(`Error removeById(id):\n${err}`)
		}
	}

	async update(song) {
		try {
			const up_song = await this.db.Song.findOne({ where: { id: song.id } })
			if (!up_song) { return false; }

			let params = {};

			if(song.title) { params.title = song.title; }

			if(song.year > 0) {
				const year = await this.db.Year.findOne({ where: { year: song.year } });
				const year_id = year == null ? (await this.db.Year.create({ year: song.year, })).dataValues.id : year.dataValues.id;
				params.yearId = year_id;
			}

			let isUpdate = false;
			if(song.musician) {
				const musician = await this.db.Musician.findOne({ where: { name: song.musician } });
				const musician_id = musician == null ? (await this.db.Musician.create({
					name: song.musician,
					birthday: new Date(),
					countryId: 2,
				})).dataValues.id : musician.dataValues.id;

				if (song.album) {
					const album = await this.db.Album.findOne({ where: { title: song.album, musicianId: musician_id } });
					const album_id = album == null ? (await this.db.Album.create({
						title: song.album,
						yearId: up_song.yearId,
						musicianId: musician_id,
					})).dataValues.id : album.dataValues.id;
					params.albumId = album_id;
					isUpdate = true;
				} else {
					const album = await this.db.Album.update({
						musicianId: musician_id,
					}, { where: { id: up_song.albumId } });
				}
			}

			if (!isUpdate && song.album) {
				const album = await this.db.Album.update({
					title: song.album,
				}, { where: { id: up_song.albumId } });
			}

			const res = await this.db.Song.update(params, { where: { id: song.id } });
			if(res == 1) return true;
			else return false;
		} catch (err) {
			console.log(`Error insert(song):\n${err}`)
		}
	}

	// help functions
	params(id = -1) {
		let params = {
			attributes: ['id', ['title', 'song']],
			raw: true,
			include: [
				{
					model: this.db.Album,
					attributes: ['title'],
					required: true,
					include: [
						{
							model: this.db.Musician,
							attributes: ['name'],
							required: true,
						}
					]
				},
				{
					model: this.db.Year,
					attributes: ['year'],
					required: true,
				},
				{
					model: this.db.Genre,
				}
			],
			order: [
				['id', 'ASC'],
			],
		};
		if(id != -1) {
			params.where = {
				id: id,
			};
		}
		return params;
	}

}

module.exports = Music;

/*======================================================*/
// Private Functions
/*======================================================*/

// Return table songs
async function parseSongs(data) {
	const rows 	= data.length;
	const songs = [];
	if (rows > 0) {
		let lastSongId = -1;
		let lastId = -1;
		for (let i = 0; i < rows; i++) {
			if (lastSongId == data[i].id) {
				songs[lastId].genres.push(data[i]['genres.genre']);
			} else {
				lastId++;
				lastSongId = data[i].id;
				const song = new Song(lastSongId, data[i]['album.musician.name'], data[i]['album.title'], data[i].song, data[i]['year.year'], [data[i]['genres.genre']]);
				songs.push(song);
			}
		}
		return await songs;
	} else {
		return [];
	}
}
