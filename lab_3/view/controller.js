// Controller

const Table = require('as-table');
const Colors = require('colors');

const Config = require('../configs/config');
const Music = require('../database/music')

const Song = require('../models/song');

/*=====================================================================*/

const db = new Music();

async function start() {
	await db.start(Config);
}
start();

module.exports = {
	
	async music() {
		const music = await db.getAll();
		print(music);
	},

	async song(id) {
		const song = await db.getById(id);
		print(song);
	},

	async songId(id) {
		const song = await db.getById(id);
		return song;
	},

	async remove(id) {
		const res = await db.removeById(id);
		if (res > 0) { console.log(`\nSuccessfully deleting an item with id: ${id}\n`.green); }
		else { console.log(`\nIt is impossible to delete an element with id ${id}\n`.red); }
	},

	async insert(song) {
		const id = await db.insert(song);
		if(id === 0) {
			console.log(`\nCan't delete song\n`.red);
		} else {
			console.log(`\nSuccess! New song id: ${id}\n`.green);
			const song = await db.getById(id);
			print(song);
		}
	},

	async update(song) {
		const isUpdate = await db.update(song);
		if(!isUpdate) {
			console.log(`\nSomething went wrong.\nCan't update song\n`.red);
		} else {
			console.log(`\nSuccess! Updated song.\n`.green);
			const up_song = await db.getById(song.id);
			print(up_song);
		}
	}
};


/*=====================================================================*/

const tableConfig = {
	delimiter: '  ▏ '.red,
	dash: '⎯'.red,
	left: true,
	title: x => x.yellow,
	print: x => (typeof x === 'number') ? String(x).red : String(x)
};

function print(date) {
	if (date.length > 0) { console.log(`\n${Table.configure(tableConfig)(date)}\n`); }
	else { console.log('\nThere is nothing!\nTry again.\n'.red) }
}

function printExplain(explain) {
	console.log(`\n=> ${explain.green}\n`.green);
}