// Controller

const Table = require('as-table');
const Colors = require('colors');

const config = require('../configs/config');
const Storage = require('../storage/storage'); 
const Song = require('../models/song');

/*=====================================================================*/


const db = new Storage(config);

module.exports = {
	
	async music() {
		const songs = await db.getSongs();
		print(songs);
	},

	async song(id) {
		const song = await db.getSongById(id);
		print(song);
	},

	async songId(id) {
		return await db.getSongById(id);
	},

	async remove(id) {
		const res = await db.delSongById(id);
		if (res > 0) { console.log(`\nSuccessfully deleting an item with id: ${id}\n`.green); }
		else { console.log(`\nIt is impossible to delete an element with id ${id}\n`.red); }
	},

	async generation(count) {
		for (let i = 0; i < count; i++) {
			console.clear();
			console.log(`add: ${i + 1}`.green);
			await db.generatingSong();
		}
	},

	async search(params) {
		const songs = await db.searchSong(params);
		print(songs.table);
		printExplain(songs.explain);
	},

	async insert(song) {
		const id = await db.addSong(song);
		if(id === 0) {
			console.log(`\nCan't delete song\n`.red);
		} else {
			console.log(`\nSuccess! New song id: ${id}\n`.green);
			const song = await db.getSongById(id);
			print(song);
		}
	},

	async update(song) {
		const isUpdate = await db.updateSong(song);
		if(!isUpdate) {
			console.log(`\nSomething went wrong.\nCan't update song\n`.red);
		} else {
			console.log(`\nSuccess! Updated song.\n`.green);
			const track = await db.getSongById(song.id);
			print(track);
		}
	}
};


/*=====================================================================*/

const tableConfig = {
	delimiter: '  ▏ '.red,
	dash: '⎯'.red,
	left: true,
	title: x => x.yellow,
	print: x => (typeof x === 'number') ? String(x).red : String(x)
};

function print(date) {
	if (date.length > 0) { console.log(`\n${Table.configure(tableConfig)(date)}\n`); }
	else { console.log('\nThere is nothing!\nTry again.\n'.red) }
}

function printExplain(explain) {
	console.log(`\n=> ${explain.green}\n`.green);
}