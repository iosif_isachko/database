// App


/*=======================================================
Require
=========================================================*/

const Controller = require('./view/controller');
const Router = require('./view/router');
const Song = require('./models/song');

const Colors = require('colors');

/*=======================================================
Global Variables
=========================================================*/

const commands = {
	music_a: 'music -a',
	music_id: 'music -id',
	music_d: 'music -d',
	music_g: 'music -g',
	music_s: 'music -s',
	music_i: 'music -i',
	music_u: 'music -u',
	music_man: 'man music',
	exit: 'exit',
};

let params = null;
let song = null;
let paramsStatus = 1;

/*=======================================================
Router
=========================================================*/

const router = new Router();
router.use(commands.music_a, Controller.music);
router.use(commands.music_id, Controller.song);
router.use(commands.music_d, Controller.remove);
router.use(commands.music_g, Controller.generation);
router.use(commands.music_s, Controller.search);
router.use(commands.music_i, Controller.insert);
router.use(commands.music_u, Controller.update);
router.use(commands.music_man, manMusic);

/*=======================================================
Functions
=========================================================*/

// Start listening for commands from the console
function startOnCommand() {
	paramsStatus = 1;
	process.stdin.removeListener('data', onSearch);
	process.stdin.removeListener('data', onInsert);
	process.stdin.removeListener('data', onUpdate);
	process.stdin.on('data', onCommand);
	process.stdout.write('\nEnter command: '.cyan);
}

// Start listening for parameters from the console
function startOnSearch() {
	params = { musician: "", album: "", title: "", year: [],};
	/*-------------------------------------------------------*/
	process.stdin.removeListener('data', onCommand);
	process.stdin.on('data', onSearch);
	console.log('\n---'.yellow);
	process.stdout.write('Enter musician (default any): '.yellow);
}

function startOnInsert() {
	song = new Song(0, "", "", "", 0, []);
	/*-------------------------------------------------------*/
	process.stdin.removeListener('data', onCommand);
	process.stdin.on('data', onInsert);
	console.log('\n---'.yellow);
	process.stdout.write('Enter musician: '.yellow);
}

async function startOnUpdate(id) {
	const songs = await Controller.songId(id);
	/*-------------------------------------------------------*/
	if(songs.length === 0) {
		console.log(`\nError while searching for song under id: ${id}\n`.red);
		process.stdout.write('\nEnter command: '.cyan);
	} else {
		song = songs[0];
		process.stdin.removeListener('data', onCommand);
		process.stdin.on('data', onUpdate);
		console.log('\n---'.yellow);
		process.stdout.write(`Enter musician (default '${song.musician}'): `.yellow);
	}
}

function endProcess() {
	console.log("\nBye!\n".yellow);
	process.exit(0);
}

// Console Command Processing
async function onCommand(data) {
	/*-------------------------------------------------------*/
	const text = data.toString().trim();
	const parts = text.split(' ');
	const command = (parts.slice(0, 2)).join(' ');
	/*-------------------------------------------------------*/
	let input = (parts.slice(2, 3)).join(' ');
	if (((command === commands.music_id && isNaN(input)) || (command === commands.music_id && !input)) ||
		((command === commands.music_d && isNaN(input)) || (command === commands.music_d && !input)) ||
		((command === commands.music_g && isNaN(input)) || (command === commands.music_g && !input)) ||
		((command === commands.music_u && isNaN(input)) || (command === commands.music_u && !input))
	) {
		console.log(`\nError.\nUnsuccessful command '${text}'.\nUse: '${commands.music_man}'.\n`.red);
		process.stdout.write('Enter command: '.cyan);
	}
	/*-------------------------------------------------------*/
	else {
		console.clear();
		if (command === commands.exit || command === '') { endProcess(); }
		else if (command === commands.music_s) { startOnSearch(); }
		else if (command === commands.music_i) { startOnInsert(); }
		else if (command === commands.music_u) { startOnUpdate(input); }
		else {
			await router.handle(command, input);
			process.stdout.write('Enter command: '.cyan);
		}
	}
}

// Console Search Processing
async function onSearch(data) {
	/*-------------------------------------------------------*/
	const text = data.toString().trim();
	/*-------------------------------------------------------*/
	
	if (paramsStatus === 1) {
		params.musician = text;
		process.stdout.write('Enter album (default any): '.yellow);
		paramsStatus++;
	}
	else if (paramsStatus === 2) {
		params.album = text;
		process.stdout.write('Enter title (default any): '.yellow);
		paramsStatus++;
	}
	else if (paramsStatus === 3) {
		params.title = text;
		process.stdout.write('Year or range start and end. Delimiter space\nEnter year (default any): '.yellow);
		paramsStatus++;
	}
	else if (paramsStatus === 4) {
		let isNumber = true;
		if (text) {

			const parts = text.split(' ');
			if (isNaN(parts[0])) { isNumber = false; }
			else { params.year.push(Number.parseInt(parts[0])); }

			if (parts[1]) { 
				if (isNaN(parts[1])) { isNumber = false; }
				else { params.year.push(Number.parseInt(parts[1])); }
			}
		}

		if(isNumber) {
			await router.handle(commands.music_s, params);
			startOnCommand();
		} else {
			params.year = [];
			console.log(`\nError: '${text}'.\nTry to enter the value again.\n`.red);
			process.stdout.write('again: '.yellow);
		}
	}
}

// Console Insert Processing
async function onInsert(data) {
	/*-------------------------------------------------------*/
	const text = data.toString().trim();
	/*-------------------------------------------------------*/

	if (paramsStatus === 1) {
		song.musician = text;
		process.stdout.write('Enter album: '.yellow);
		paramsStatus++;
	}
	else if (paramsStatus === 2) {
		song.album = text;
		process.stdout.write('Enter title: '.yellow);
		paramsStatus++;
	}
	else if (paramsStatus === 3) {
		song.title = text;
		process.stdout.write('Enter year: '.yellow);
		paramsStatus++;
	}
	else if (paramsStatus === 4) {
		if(!isNaN(text)) {
			song.year = Number.parseInt(text);
			process.stdout.write('Enter genres (delimiter space): '.yellow);
			paramsStatus++;
		} else {
			console.log(`\nError: '${text}'.\nTry to enter the value again.\n`.red);
			process.stdout.write('again: '.yellow);
		}
	}
	else if (paramsStatus === 5) {
		const parts = text.split(' ');
		for (const genre of parts) { song.genres.push(genre); }
		await router.handle(commands.music_i, song);
		startOnCommand();
	}
}

// Console Update Processing
async function onUpdate(data) {
	/*-------------------------------------------------------*/
	const text = data.toString().trim();
	/*-------------------------------------------------------*/

	if (paramsStatus === 1) {
		if (text != '') { song.musician = text; }
		process.stdout.write(`Enter album (default '${song.album}'): `.yellow);
		paramsStatus++;
	}
	else if (paramsStatus === 2) {
		if (text != '') { song.album = text; }
		process.stdout.write(`Enter title (default '${song.title}'): `.yellow);
		paramsStatus++;
	}
	else if (paramsStatus === 3) {
		if (text != '') { song.title = text; }
		process.stdout.write(`Enter year (default '${song.year}'): `.yellow);
		paramsStatus++;
	}
	else if (paramsStatus === 4) {
		if (text != '' && isNaN(text)) {
			console.log(`\nError: '${text}'.\nTry to enter the value again.\n`.red);
			process.stdout.write('again: '.yellow);
		} else {
			if (text != '') { song.year = Number.parseInt(text); }
			process.stdout.write(`Enter genres delimiter space (default '${song.genres.join(', ')}'): `.yellow);
			paramsStatus++;
		}
	}
	else if (paramsStatus === 5) {
		if (text != '') { 
			const parts = text.split(' ');
			song.genres = [];
			for (const genre of parts) { song.genres.push(genre); }
		}
		await router.handle(commands.music_u, song);
		startOnCommand();
	}

}

// Man
function manMusic() {
	console.log('\n--------------------------------------------\n'.magenta);
	console.log(`${commands.music_a}          : display all music`.magenta);
	console.log(`${commands.music_id} {id}    : find a song by id`.magenta);
	console.log(`${commands.music_d} {id}     : delete song by id`.magenta);
	console.log(`${commands.music_s}          : music search`.magenta);
	console.log(`${commands.music_g} {count}  : generating music`.magenta);
	console.log(`${commands.music_i}          : insert new song`.magenta);
	console.log(`${commands.music_u} {id}     : update song by id`.magenta);
	console.log(`${commands.exit}              : quit the application`.magenta);
	console.log('\n--------------------------------------------\n'.magenta);
}

/*=======================================================
Main
=========================================================*/

console.clear();
startOnCommand();

/*--------------------------END--------------------------*/