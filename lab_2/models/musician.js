// Model: Musician

class Musician 
{
	constructor (id, name, country, birthday) {
		this.id = id;
		this.name = name;
		this.country = country;
		this.birthday = birthday;
	}
}

module.exports = Musician;