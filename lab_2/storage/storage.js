//

const Song 		= require('../models/song');
const Database 	= require('../models/database');



class StorageMusic {

	constructor(config) {
		this.db = new Database(config);
	}

	async getSongs() {
		const query 	= `SELECT * FROM select_song ORDER BY id`;
		const res 		= await this.db.request(query);
		return await parseSongs(res);
	}

	async getSongById(id) {
		const query 	= `SELECT * FROM select_song WHERE id = ${id}`;
		const res 		= await this.db.request(query);
		return await parseSongs(res);
	}

	async delSongById(id) {
		const query 	= `DELETE FROM songs WHERE id = ${id}`;
		const res 		= await this.db.request(query);
		return res.rowCount;
	}

	async addSong(song) {
		// song.id, song.musician, song.album, song.title, song.year, song.genres [];

		const album_id 		= await getAlbumId(this.db, song.album, song.musician);
		const year_id 		= await getYearId(this.db, song.year);

		// Check if such a track exists
		let query 			= `SELECT id FROM songs WHERE album_id = ${album_id} AND year_id = ${year_id} AND lower(title) = lower('${song.title}')`;
		let res 			= await this.db.request(query);

		if(res.rowCount == 1) { return 0; }

		query 				= `INSERT INTO songs(album_id, year_id, title) VALUES(${album_id}, ${year_id}, '${song.title}') RETURNING id`;
		res 				= await this.db.request(query);
		const song_id 		= res.rows[0].id;

		for(const genre of song.genres) {
			let genre_id 	= await getGenreId(this.db, genre);
			query 			= `INSERT INTO sg(song_id, genre_id) VALUES(${song_id}, ${genre_id})`;
			await this.db.request(query);
		}

		return await song_id;
	}

	async updateSong(song)
	{
		let isUpdate = false;
		const song_id 	= song.id;
		let query 		= `SELECT * FROM songs WHERE id = ${song_id}`;
		let res 		= await this.db.request(query);
		if (res.rowCount == 0) { return isUpdate; }

		const album_id 	= await getAlbumId(this.db, song.album, song.musician);
		const year_id 	= await getYearId(this.db, song.year);

		query 			= `SELECT id FROM songs WHERE album_id = ${album_id} AND year_id = ${year_id} AND lower(title) = lower('${song.title}')`;
		res = await this.db.request(query);
		if(res.rowCount == 1) { 
			if (song_id != res.rows[0].id) { return isUpdate; }
		} else {
			isUpdate = true;
		}

		if(isUpdate) {
			query 		= `UPDATE songs SET album_id = ${album_id}, year_id = ${year_id}, title = '${song.title}' WHERE id = ${song_id};`;
			res 		= await this.db.request(query);
			if (res.rowCount == 0) { isUpdate = false; }
		}

		// Update genres song
		query 		= `SELECT * FROM sg WHERE song_id = ${song_id}`;
		res 		= await this.db.request(query);
		const sg 	= res.rows;
		if (sg.length === 0) {
			if (song.genres.length != 0) { isUpdate = true; }
			for (const genre of song.genres) {
				let genre_id = await getGenreId(this.db, genre);
				query = `INSERT INTO sg(song_id, genre_id) VALUES(${song_id}, ${genre_id})`;
				await this.db.request(query);
			}
		} else {
			const genres = [];
			for (const g of song.genres) {
				genres.push(await getGenreId(this.db, g));
			}
			for(const item of sg) {
				let isset = false;
				for (let i = 0; i < genres.length; i++) {
					if (item.genre_id == genres[i]) { 
						isset = true; 
						genres[i] = null;
					}
				}
				if(!isset) {
					isUpdate = true;
					query = `DELETE FROM sg WHERE song_id = ${song_id} AND genre_id = ${item.genre_id}`;
					await this.db.request(query);
				}
			}
			for(const g of genres) {
				if(g != null) {
					query = `INSERT INTO sg(song_id, genre_id) VALUES(${song_id}, ${g})`;
					await this.db.request(query);
					isUpdate = true;
				}
			}
		}
		return isUpdate;
	}

	async searchSong(params) {
		let query = createRequest(params);
		console.log();
		console.log(query);
		console.log();
		const res = await this.db.request(query);
		const explain = await this.executionTime(query);
		return {
			table: await parseSongs(res),
			explain: explain,
		};
	}

	async executionTime(query) {
		query = `EXPLAIN ANALYZE ${query}`;
		const res = await this.db.request(query);
		return res.rows[Object.keys(res.rows).length - 1]['QUERY PLAN'];
	}

	async generatingSong() {
		const country_id = (await generationCountry(this.db)).rows[0].id;
		const musician_id = (await generationMusician(this.db, country_id)).rows[0].id;
		const album_id = (await generationAlbum(this.db, await getYearId(this.db, random(1980, 2020)), musician_id)).rows[0].id;
		const song_id = (await generationSong(this.db, await getYearId(this.db, random(1981, 2020)), album_id)).rows[0].id;
		const query = `INSERT INTO sg(song_id, genre_id) VALUES(${song_id}, 1)`;
		await this.db.request(query);
	}
}



/*======================================================*/
// Private Functions
/*======================================================*/

// Return table songs
async function parseSongs(data) {
	const rows 	= data.rows;
	const songs = [];
	if(rows.length) {
		for (const item of rows) {
			const song = new Song(item.id, item.musician, item.album, item.title, item.year, item.genres);
			songs.push(song);
		}
		return songs;
	} else {
		return [];
	}
}

// Search year (or create new). Return year id
async function getYearId(db, year)
{
	let query 		= `SELECT id FROM years WHERE year = ${year}`;
	let res 		= await db.request(query);
	let year_id 	= res.rowCount == 1 ? res.rows[0].id : null;

	if (!year_id) {
		query 		= `INSERT INTO years(year) VALUES (${year}) RETURNING id`;
		year_id 	= (await db.request(query)).rows[0].id;
	}

	return await year_id;
}

// Search genre (or create new). Return genre id
async function getGenreId(db, genre) {
	let query 		= `SELECT id FROM genres WHERE lower(genre) = lower('${genre}')`;
	let res 		= await db.request(query);
	let genre_id 	= res.rowCount == 1 ? res.rows[0].id : null;

	if (!genre_id) {
		query 		= `INSERT INTO genres(genre) VALUES ('${genre}') RETURNING id`;
		genre_id 	= (await db.request(query)).rows[0].id;
	}

	return await genre_id;
}

// Search album (or create new). Return album id
async function getAlbumId(db, album, musician)
{
	let query 		= `SELECT albums.id as album_id, musicians.id as musician_id FROM albums INNER JOIN musicians ON albums.musician_id = musicians.id WHERE lower(albums.title) = lower('${album}') AND lower(musicians.name) = lower('${musician}')`;
	let res 		= await db.request(query);
	let album_id 	= res.rowCount == 1 ? res.rows[0].album_id : null;

	if (!album_id) {
		query 			= `SELECT id FROM musicians WHERE lower(name) = lower('${musician}')`;
		res 			= await db.request(query);
		let musician_id = res.rowCount == 1 ? res.rows[0].id : null;

		if (!musician_id) {
			query 		= `INSERT INTO musicians(name) VALUES ('${musician}') RETURNING id`;
			musician_id = (await db.request(query)).rows[0].id;
		}
		query = `INSERT INTO albums(musician_id, title) VALUES ('${musician_id}', '${album}') RETURNING id`;
		album_id = (await db.request(query)).rows[0].id;
	}

	return await album_id;
} 

// Create search query, from parameters
function createRequest(params) 
{
	let isWhere = false;
	let query = `SELECT * FROM select_song`;

	if (params.musician != "") {
		if (!isWhere) { query += ` WHERE`; isWhere = true; }
		else { query += ` AND`; }
		query += ` lower(musician) LIKE '%${params.musician.toLowerCase()}%'`;
	}

	if (params.album != "") {
		if (!isWhere) { query += ` WHERE`; isWhere = true; }
		else { query += ` AND`; }
		query += ` lower(album) LIKE '%${params.album.toLowerCase()}%'`;
	}

	if (params.title != "") {
		if (!isWhere) { query += ` WHERE`; isWhere = true; }
		else { query += ` AND`; }
		query += ` lower(title) LIKE '%${params.title.toLowerCase()}%'`;
	}

	if (params.year.length != 0) {
		if (!isWhere) { query += ` WHERE`; isWhere = true; }
		else { query += ` AND`; }

		if (params.year.length == 1) {
			query += ` year = ${params.year[0]}`;
			query += ` ORDER BY id`;
		} else {
			query += ` year BETWEEN ${params.year[0]} AND ${params.year[1]}`;
			query += ` ORDER BY year`;
		}

	} else { query += ` ORDER BY id`; }

	return query;
}



/*======================================================*/
// Generation
/*======================================================*/

const generate = {
	char: `chr(trunc(65+random()*25)::int)`,
	birthday: `timestamp '1990-01-10 20:00:00' + random() * (timestamp '2005-01-20 20:00:00' - timestamp '1990-01-10 10:00:00')`,
};

// Random 
function random(min, max) {
	min = Math.ceil(min);
	max = Math.floor(max);
	return Math.floor(Math.random() * (max - min + 1)) + min;
}

// Generation request string
function generationRequestString(max = 2) {
	let req = `${generate.char} || lower(${generate.char})`;
	for (let i = 2; i < max; i++) {
		req += ` || lower(${generate.char})`;
	}
	return req;
}

async function generationCountry(db) {
	const query = `INSERT INTO countries(country) VALUES(${generationRequestString(random(5, 32))}) RETURNING id`;
	let res = await db.requestWithoutCatch(query);
	if (!res) { res = await generationCountry(db); }
	return await res;
}

async function generationMusician(db, country_id) {
	const query = `INSERT INTO musicians(country_id, name, birthday) VALUES(${country_id}, ${generationRequestString(random(20, 32))}, ${generate.birthday}) RETURNING id`;
	let res = await db.requestWithoutCatch(query);
	if (!res) { res = await generationMusician(db, country_id); }
	return await res;
}

async function generationAlbum(db, year_id, musician_id) {
	const query = `INSERT INTO albums(year_id, musician_id, title) VALUES(${year_id}, ${musician_id}, ${generationRequestString(random(15, 32))}) RETURNING id`;
	let res = await db.requestWithoutCatch(query);
	if (!res) { res = await generationAlbum(db, year_id, musician_id); }
	return await res;
}

async function generationSong(db, year_id, album_id) {
	const query = `INSERT INTO songs(year_id, album_id, title) VALUES(${year_id}, ${album_id}, ${generationRequestString(random(10, 32))}) RETURNING id`;
	let res = await db.requestWithoutCatch(query);
	if (!res) { res = await generationSong(db, year_id, album_id); }
	return await res;
}




module.exports = StorageMusic;